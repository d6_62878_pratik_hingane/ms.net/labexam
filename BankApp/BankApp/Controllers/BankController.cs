﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BankApp.Controllers
{
    database dbHelper = new database();

    public ActionResult Index()
    {
        List<database> users = dbHelper.databases.ToList();
        return View("Index", users);
    }
    public ActionResult Approve(int id)
    {
        database database = dbHelper.databases.Find(id);
        database.Status = "Approved";
        dbHelper.SaveChanges();
        return Redirect("/Bank/Index");
    }
    public ActionResult Add()
    {
        return View("Add");
    }
    public ActionResult AfterAdd(database database)
    {
        dbHelper.databases.Add(database);
        dbHelper.SaveChanges();
        return Redirect("Index");
    }
    public ActionResult Reject(int id)
    {
        databases databases = dbHelper.databases.Find(id);
        databases.Status = "Rejected";
        dbHelper.SaveChanges();
        return Redirect("/Bank/Index");
    }
    public ActionResult Delete(int id)
    {
        database database = dbHelper.databases.Find(id);
        dbHelper.databases.Remove(database);
        dbHelper.SaveChanges();
        return Redirect("/Bank/Index");
    }

}
}
}database
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using BankApp.Models;

namespace BankApp.Controllers
{
    public class LoginController : Controller
    {
        private object database;

        // GET: Login
        public ActionResult Signin()
        {
            return View("Login");
        }

        public ActionResult AfterSignin(databaseEntities Data, string ReturnUrl)
        {
            FormsAuthentication.SetAuthCookie(database.UserName, false);
            if (CheckWithDb(database))

            {

                if (ReturnUrl != null)
                {
                    return Redirect(ReturnUrl);
                }
                else
                {
                    return Redirect("/Data/Index");
                }
            }
            else
            {
                ViewBag.InvalidMessage = "UserName Or PassWord is not Valid ";
                return View("Signin");
            }

        }
        private bool CheckWithDb(database Data)
        {
            List<database> Users = dbHelper.databases.ToList();
            foreach (database User in Users)
            {
                if (User.PersonName.ToLower() == database.UserName.ToLower() && User.Password == Data.Password)
                {
                    return true;
                }
            }
            return false;
        }
        public ActionResult SignOut()
        {
            FormsAuthentication.SignOut();
            return Redirect("/Home/Index");
        }

    }
}
    }
}